#include "entry.h"
#include "stringifier.h"

namespace kara
{

std::ostream& operator<<(std::ostream& out, const Entry& entry)
{
    out << '{' << std::endl;

    for (const auto& p : entry)
    {
        out << p.first << " = " << std::visit(Stringifier<QuoteDecorator>(), p.second) << ',' << std::endl;
    }

    out << '}';

    return out;
}

std::ostream& operator<<(std::ostream& out, const Entries& entries)
{
    for (const auto& entry : entries)
    {
        out << entry << std::endl;
    }

    return out;
}
    
} /* namespace kara */