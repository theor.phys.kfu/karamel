#include <iostream>
#include "application.h"


int main(int argc, char *argv[])
{
    kara::fs::path fileObjects = "../configs/objects.kara";
    kara::fs::path fileConfig = "../configs/config.cf";

    if (argc > 1)
        fileObjects = argv[1];

    if (argc > 2)
        fileConfig = argv[2];

    try
    {
        kara::Application app(fileObjects, fileConfig);

        app.run();

    } catch (const std::runtime_error& msg)
    {
        std::cout << msg.what() << std::endl;

        return EXIT_FAILURE;
    }

    return 0;
}
