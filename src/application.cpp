#include "application.h"
#include "code_generator.h"
#include "config_parser.h"
#include "kara_parser.h"

#include <iostream>
#include <fstream>
#include <sstream>

#include <boost/format.hpp>

namespace kara
{

const std::vector<std::string_view>& Application::macroNames()
{
    return CodeGenerator::macroNames();
}

const std::vector<std::string_view>& Application::reservedPlaceholders()
{
    return CodeGenerator::reservedPlaceholders();
}

bool Application::loadConfigs(const fs::path& fileConfig)
{
    std::ifstream in(fileConfig);

    if (!in.is_open())
    {
        return false;
    }

    std::ostringstream ss;
    ss << in.rdbuf();

    std::string str = ss.str();

    parser::config_parse(str.begin(), str.end(), mConfigList);

    return true;
}

bool Application::loadKaraObjects(const fs::path& fileKara)
{
    std::ifstream in(fileKara);

    if (!in.is_open())
    {
        return false;
    }

    std::ostringstream ss;
    ss << in.rdbuf();

    std::string str = ss.str();

    parser::kara_parse(str.begin(), str.end(), mEntries);

    return true;
}

Application::Application(const fs::path& fileObjects, const fs::path& fileConfig)
{
    if (!loadKaraObjects(fileObjects))
    {
        boost::format format = boost::format("KaraObjectsList: File %1% not found!") % fileObjects;
    
        throw std::runtime_error(format.str());
    }

    if (!loadConfigs(fileConfig))
    {
        boost::format format = boost::format("CodeGenConfig: File %1% not found!") % fileConfig;    
    
        throw std::runtime_error(format.str());
    }

    std::cout << boost::format("KaraObjects count: %1%\n") % mEntries.size();
    std::cout << boost::format("Configs entry count: %1%\n") % mConfigList.size();
}

void Application::run() const
{
    for (const auto & config : mConfigList)
    {
        if (!generate(config))
        {
            std::cout << boost::format("Failed to generate file %1%\n") % config.destinationPath;
        }
    }
}

bool Application::generate(const CodeGenConfig& config) const
{
    std::ifstream in(config.templatePath);

    if (!in.is_open())
    {
        return false;
    }

    std::ostringstream ss;
    ss << in.rdbuf();

    std::string str = ss.str();

    CodeGenerator gen(mEntries);

    std::ofstream out(config.destinationPath);

    if (!out.is_open())
    {
        return false;
    }

    out << gen(str) << std::endl;

    return true;
}

} /* namespace kara */