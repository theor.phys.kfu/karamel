#include "code_generator.h"

#include "macro_parser.h"
#include "placeholder_parser.h"
#include "common_function.h"
#include "stringifier.h"

#include <sstream>
#include <iomanip>
#include <functional>
#include <algorithm>
#include <list>
#include <iterator>
#include <type_traits>

namespace kara
{

class PlaceholderReplacer
{
public:
    PlaceholderReplacer(constIterator begin, constIterator end,
                        const Formats& formats,
                        const std::list<Entries::const_iterator>& proxy)

        : mTemplate(removeLineBreaks(begin, end)), mFormats{formats}
    {
        mPlaceholders = parser::getPlaceholders(mTemplate.begin(), mTemplate.end());

        checkAndSetIfAutoFormat(proxy);
    }

    std::string operator()(const Entry& entry, size_t pos, size_t size) const
    {
        std::ostringstream oss;

        auto begin = mTemplate.begin();

        size_t i{0};

        for (const auto& plc : mPlaceholders)
        {
            oss << std::string(begin, plc.position.begin);

            Format format;

            if (i < mFormats.size())
            {
                format = mFormats[i++];
            }

            setFormat(oss, format);
            
            oss << replacePlaceholder(plc, entry, pos, size);

            begin = plc.position.end;
        }

        oss << std::string(begin, mTemplate.end());

        return oss.str();
    }

    static const std::vector<std::string_view>& reservedPlaceholders()
    {
        const auto getPlaceholders = []()
        {
            std::vector<std::string_view> res;
            res.reserve(Placeholder::reservedPlaceholders.size());

            for (const auto & p : Placeholder::reservedPlaceholders)
            {
                res.push_back(p.first);
            }

            return res;
        };

        static const std::vector<std::string_view> plc = getPlaceholders();

        return plc;
    }

private:
    std::string mTemplate;
    Formats mFormats;
    Placeholders mPlaceholders;

    static void setFormat(std::ostringstream& oss, const Format& format)
    {
        oss << std::setw(format.width);

        if (Align::left == format.align)
        {
            oss << std::left;
        }
        else
        if (Align::right == format.align)
        {
            oss << std::right;
        }
    }

    static std::string toString(const Placeholder& plc, const Entry& entry)
    {
        auto it = entry.find(plc.name);

        if (it == entry.end())
            return std::string(plc.position.begin, plc.position.end);

        return std::visit(Stringifier<DefaultDecorator>(plc.index), it->second);
    }

    static std::string replacePlaceholder(const Placeholder& plc, const Entry& entry, size_t pos, size_t size)
    {
        switch (plc.tag())
        {
            case PlaceholderTag::pos:
            {
                return std::to_string(pos);
            }

            case PlaceholderTag::size:
            {
                return std::to_string(size);
            }

            case PlaceholderTag::userDefined:
            {
                return toString(plc, entry);
            }

            default:
                break;
        };

        return std::string(plc.position.begin, plc.position.end);
    }

    void checkAndSetIfAutoFormat(const std::list<Entries::const_iterator>& proxy)
    {
        const auto size = std::min(mFormats.size(), mPlaceholders.size());

        for (size_t i = 0; i < size; ++i)
        {
            if (mFormats[i].isAuto)
            {
                unsigned long width{0};

                for (const auto& entryPtr : proxy)
                {
                    std::string value = replacePlaceholder(mPlaceholders[i], *entryPtr, proxy.size(), proxy.size());

                    if (value.size() > width)
                    {
                        width = value.size();
                    }
                }

                mFormats[i].width = width;
            }
        }
    }
};


const std::vector<std::string_view>& CodeGenerator::macroNames()
{
    static const std::vector<std::string_view> names =
    {
        MacroName<MacroType::K_EXPAND_FOR>::name,
        MacroName<MacroType::K_COUNT_OF>::name
    };

    return names;
}

const std::vector<std::string_view>& CodeGenerator::reservedPlaceholders()
{
    return PlaceholderReplacer::reservedPlaceholders();
}

CodeGenerator::CodeGenerator(const Entries& entries)
    : mEntries{entries}
{}

std::string CodeGenerator::operator()(const std::string& templ) const
{
    return replace(templ);
}

std::string CodeGenerator::replace(const std::string& templ) const
{
    std::list<Macro> macros = parser::getMacros(templ.begin(), templ.end());

    std::vector<ExpandedMacro> expandedMacros = getExpandedMacros(macros);

    std::vector<std::vector<std::string>> stringLists;
    stringLists.reserve(expandedMacros.size());

    size_t totalSize{0};
    size_t offset{0};
    auto begin = templ.begin();

    for (const auto& expandedMacro : expandedMacros)
    {
        offset = getOffset(begin, expandedMacro.position.begin, offset);
        totalSize += std::distance(begin, expandedMacro.position.begin);

        auto stringList = formattedStringList(expandedMacro.body, offset);
        for (const auto& line: stringList)
        {
            totalSize += line.size();
            offset = getOffset(line.begin(), line.end(), offset);
        }

        stringLists.emplace_back(std::move(stringList));
        
        begin = expandedMacro.position.end;
    }

    totalSize += std::distance(begin, templ.end());

    std::string res;
    res.reserve(totalSize);

    std::back_insert_iterator<std::string> it(res);

    begin = templ.begin();

    for (size_t i = 0; i < expandedMacros.size(); ++i)
    {
        std::copy(begin, expandedMacros[i].position.begin, it);

        for (const auto& line : stringLists[i])
        {
            std::copy(line.begin(), line.end(), it);
        }

        begin = expandedMacros[i].position.end;
    }

    std::copy(begin, templ.end(), it);

    return res;
}

ExpandedMacro CodeGenerator::getExpandedMacro(const Macro& macro) const
{
    return std::visit([&](auto&& var)->ExpandedMacro
    {
        using T = std::decay_t<decltype(var)>;

        static_assert(std::is_base_of_v<MacroBase, T>, "Type T must be derived from MacroBase type");

        std::list<Entries::const_iterator> proxy;

        for (auto it = mEntries.begin(); it != mEntries.end(); ++it)
        {
            if (isSatisfiesFilter(*it, var.filters))
            {
                proxy.emplace_back(it);
            }
        }
        
        const size_t size = proxy.size();

        if constexpr (std::is_same_v<T, MacroCountOf>)
        {
            return ExpandedMacro(var.position, std::to_string(size));
        }
        else
        if constexpr (std::is_same_v<T, MacroExpandFor>)
        {
            PlaceholderReplacer replacer(var.templatePos.begin, var.templatePos.end, var.formats, proxy);

            std::ostringstream oss;

            size_t pos{0};

            for (const auto& entryPtr : proxy)
            {
                oss << replacer(*entryPtr, pos, size);

                if (pos < size - 1)
                {
                    oss << var.delimiter << std::endl;
                }

                ++pos;
            }

            return ExpandedMacro(var.position, oss.str());
        }

    }, macro);
}

std::vector<ExpandedMacro> CodeGenerator::getExpandedMacros(const std::list<Macro>& macros) const
{
    std::vector<ExpandedMacro> res;
    res.reserve(macros.size());

    for (const auto& macro : macros)
    {
        res.push_back(getExpandedMacro(macro));
    }

    return res;
}

bool CodeGenerator::isSatisfiesFilter(const Entry& entry, const Filters& filters)
{
    if (filters.empty())
        return true;

    for (const auto& filter : filters)
    {
        auto it  = entry.find(filter.filterId);

        if (it == entry.end())
        {
            return false;
        }

        if (std::find(filter.filterValues.begin(), filter.filterValues.end(), it->second) == filter.filterValues.end())
        {
            return false;
        }
    }

    return true;
}

} /* namespace kara */