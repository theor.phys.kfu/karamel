#include "placeholder.h"

namespace kara
{

PlaceholderTag Placeholder::tag() const
{
    if (auto it = reservedPlaceholders.find(name); it != reservedPlaceholders.end())
    {
        return it->second;
    }

    return PlaceholderTag::userDefined;
}

const std::unordered_map<std::string_view, PlaceholderTag> Placeholder::reservedPlaceholders =
{
    {"pos",  PlaceholderTag::pos },
    {"size", PlaceholderTag::size}
};

} /* namespace kara */