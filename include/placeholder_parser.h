#pragma once

#include "common_parsers.h"
#include "placeholder.h"

namespace kara
{
namespace parser
{
    using x3::ulong_;
    using x3::eps;

    const x3::rule<class plc, Placeholder> plc = "placeholder";

    const auto setPlcBegin = [](auto& ctx) { _val(ctx).position.begin = _where(ctx).begin(); };

    const auto setPlcEnd = [](auto& ctx) { _val(ctx).position.end = _where(ctx).begin(); };

    const auto setIndex = [](auto& ctx) { _val(ctx).index = _attr(ctx); };

    const auto setName = [](auto& ctx) { _val(ctx).name = _attr(ctx); };

    const auto plc_def = lexeme[ eps[setPlcBegin]
    >> '$'
    >> key_word[setName]
    >> -('[' >> ulong_[setIndex] >> ']')
    >> '$' ][setPlcEnd];

    BOOST_SPIRIT_DEFINE(plc);

    template <class ForwardIterator>
    inline Placeholders getPlaceholders(ForwardIterator begin, ForwardIterator end)
    {
        Placeholders res;

        while (begin != end)
        {
            x3::phrase_parse(begin, end, *plc, x3::ascii::space, res);
                
            if (begin != end)
            {
                ++begin;
            }
        }
        
        return res;
    }

} /* namespace parser */
} /* namespace kara */