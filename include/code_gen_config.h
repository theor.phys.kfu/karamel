#pragma once 

#include <string_view>
#include <filesystem>

namespace kara
{

class CodeGenConfig
{
public:
    static constexpr std::string_view templatePathName = "template_path";
    static constexpr std::string_view destinationPathName = "destination_path";

    std::filesystem::path templatePath;
    std::filesystem::path destinationPath;
};

} /* namespace kara */