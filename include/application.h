#pragma once 

#include <filesystem>
#include <vector>
#include <list>
#include <string_view>

#include "code_gen_config.h"
#include "entry.h"

namespace kara
{

namespace fs = std::filesystem;

class Application
{
public:
    Application(const fs::path& fileObjects, const fs::path& fileConfig);

    static const std::vector<std::string_view>& macroNames();

    static const std::vector<std::string_view>& reservedPlaceholders();

    void run() const;

private:
    Entries mEntries;
    std::list<CodeGenConfig> mConfigList;

    bool loadConfigs(const fs::path& fileConfig);

    bool loadKaraObjects(const fs::path& fileKara);

    bool generate(const CodeGenConfig& config) const;
};

} /* namespace kara */