#pragma once 

#include <optional>
#include <vector>
#include <unordered_map>
#include "macro.h"

namespace kara
{

enum class PlaceholderTag
{
    pos,
    size,
    userDefined
};

class Placeholder
{
public:
    Placeholder() = default;

    PlaceholderTag tag() const;

    Position position;
    std::string name;
    std::optional<unsigned long> index;

    static const std::unordered_map<std::string_view, PlaceholderTag> reservedPlaceholders;
};

using Placeholders = std::vector<Placeholder>;

} /* namespace kara */