#pragma once

#include <string>
#include <string_view>

#include "macro.h"
#include "entry.h"

namespace kara
{

class CodeGenerator
{
public:
    CodeGenerator(const Entries& entries);

    std::string operator()(const std::string& templ) const;

    static const std::vector<std::string_view>& reservedPlaceholders();

    static const std::vector<std::string_view>& macroNames();

private:
    const Entries& mEntries;

    ExpandedMacro getExpandedMacro(const Macro& macro) const;

    std::vector<ExpandedMacro> getExpandedMacros(const std::list<Macro>& macros) const;

    std::string replace(const std::string& templ) const;

    static bool isSatisfiesFilter(const Entry& entry, const Filters& filters);
};
    
} /* namespace kara */
