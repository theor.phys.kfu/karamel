#pragma once

#include <variant>
#include <vector>
#include <string>

namespace kara
{
    
using VarT = std::variant<int,
                          double,
                          std::string,
                          std::vector<int>,
                          std::vector<double>,
                          std::vector<std::string>>;

using constIterator = std::string::const_iterator;                            

} /* namespace kara */