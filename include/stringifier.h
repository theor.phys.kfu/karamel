#pragma once

#include <optional>
#include <sstream>
#include <algorithm>
#include <type_traits>
#include "typedef.h"

namespace kara
{

struct DefaultDecorator
{
    static std::string decorate(const std::string& val)
    {
        return val;
    }
};

struct QuoteDecorator
{
    static std::string decorate(const std::string& val)
    {
        std::string res(val.size()+2, '"');
        std::copy(val.begin(), val.end(), res.begin()+1);

        return res;
    }
};

template <class Decorator = DefaultDecorator>
class Stringifier
{
public:
    Stringifier() = default;

    Stringifier(unsigned long index)
        : mIndex{index}
    {}

    Stringifier(const std::optional<unsigned long>& index)
        : mIndex{index}
    {}

    std::string operator()(const std::string& val) const
    {
        return Decorator::decorate(val);
    }

    template <class T>
    std::string operator()(const T& val) const
    {
        static_assert(std::is_integral_v<T> || std::is_floating_point_v<T>, "Type T must be real or integer");

        std::ostringstream oss;

        oss << val;

        return oss.str();
    }

    std::string operator()(const std::vector<std::string>& val) const
    {
        if (mIndex)
        {
            return Decorator::decorate( val[*mIndex] );
        }

        std::ostringstream oss;

        oss << '{';

        for (size_t i = 0; i < val.size()-1; ++i)
        {
            oss << Decorator::decorate( val[i] ) << ',';
        }

        oss << Decorator::decorate( val.back() );

        oss << '}';

        return oss.str();
    }

    template <class T>
    std::string operator()(const std::vector<T>& val) const
    {
        static_assert(std::is_integral_v<T> || std::is_floating_point_v<T>, "Type T must be real or integer");

        std::ostringstream oss;

        if (mIndex)
        {
            oss << val[*mIndex];
            return oss.str();
        }

        oss << '{';

        for (size_t i = 0; i < val.size()-1; ++i)
        {
            oss << val[i] << ',';
        }

        oss << val.back();

        oss << '}';

        return oss.str();
    } 

private:
    std::optional<unsigned long> mIndex;
};

} /* namespace kara */