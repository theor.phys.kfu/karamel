#pragma once 

#include <cstddef>
#include <string>
#include <vector>
#include <iterator>
#include <algorithm>
#include <boost/xpressive/xpressive.hpp>

namespace kara
{
template <class ForwardIterator>
inline size_t lineCount(ForwardIterator begin, ForwardIterator end)
{
    size_t n{0};

    if (begin == end)
        return n;

    while (begin != end)
    {
        auto it = std::find(begin, end, '\n');
        ++n;
        if (it == end)
            return n;
    
        begin = ++it;
    }

    return n;
}

inline size_t lineCount(const std::string& src)
{
    return lineCount(src.begin(), src.end());
}

template <class ForwardIterator>
inline std::vector<std::string> formattedStringList(ForwardIterator begin, ForwardIterator end, size_t offset)
{
    std::vector<std::string> res;
    res.reserve(lineCount(begin, end));

    auto it = std::find(begin, end, '\n');
        
    if (it == end)
    {
        res.emplace_back(begin, it);
        return res;
    }

    res.emplace_back(begin, ++it);

    if (it == end)
        return res;

    begin = it;

    for (;;)
    {
        it = std::find(begin, end, '\n');
        
        if (it == end)
        {
            std::string s(offset + std::distance(begin, it), ' ');
            std::copy(begin, it, s.begin() + offset);
            res.emplace_back(std::move(s));
            break;
        }
        std::string s(offset + std::distance(begin, ++it), ' ');
        std::copy(begin, it, s.begin() + offset);
        res.emplace_back(std::move(s));
        begin = it;
    }

    return res;
}

inline std::vector<std::string> formattedStringList(const std::string& src, size_t offset)
{
    return formattedStringList(src.begin(), src.end(), offset);
}

template <class ForwardIterator>
inline size_t getOffset(ForwardIterator begin, ForwardIterator end, size_t offset = 0)
{
    for (; begin != end; ++begin)
    {
        ++offset;
        if (*begin == '\n')
        {
            offset = 0;
        }
    }
    return offset;
}

inline size_t getOffset(const std::string& src)
{
    return getOffset(src.begin(), src.end());
}

template <class ForwardIterator>
inline std::string removeLineBreaks(ForwardIterator begin, ForwardIterator end)
{
    using namespace boost::xpressive;

    sregex regex = '\\' >> *space >> _n;

    return regex_replace(std::string(begin, end), regex, "");
}

} /* namespace kara */