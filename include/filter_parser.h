#pragma once 

#include "common_parsers.h"
#include "filter.h"

namespace kara
{
namespace parser
{
    const x3::rule<class filter, Filter> filter = "filter";
    const x3::rule<class filters, Filters> filters = "filters";

    const auto setFilterId = [](auto& ctx) { _val(ctx).filterId = _attr(ctx); };

    const auto addFilterValue = [](auto& ctx) { _val(ctx).filterValues.emplace_back(_attr(ctx)); };

    const auto filter_def = key_word[setFilterId] >> '=' >> '{' >> (var[addFilterValue] % ',') >> *lit(',') >> '}';

    const auto filters_def = filter % ',' >> -lit(',');

    BOOST_SPIRIT_DEFINE(filter, filters);

} /* namespace parser */
} /* namespace kara */