#pragma once 

#include <vector>

namespace kara
{

enum class Align
{
    left,
    right
};

class Format
{
public:
    Format()
        : width{0}, align{Align::left}, isAuto{true}
    {}

    Format(unsigned long _width, Align _align, bool _auto = true)
        : width{_width}, align{_align}, isAuto{_auto}
    {}

    unsigned long width;
    Align align;
    bool isAuto;
};

using Formats = std::vector<Format>;

} /* namespace kara */