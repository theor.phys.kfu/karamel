#pragma once 

#include <boost/fusion/adapted/std_pair.hpp>
#include "common_parsers.h"
#include "code_gen_config.h"

namespace kara
{
namespace parser
{
    const x3::rule<class fspath, std::string> fspath = "fspath";
    const x3::rule<class record, std::pair<std::string, std::string>> record = "record";
    const x3::rule<class kara_config, CodeGenConfig> kara_config = "kara_config";
    
    const auto fspath_def = lexeme[ *(alnum | char_("-_:/.+=\\")) ];

    const auto record_def = key_word >> '=' >> '"' >> fspath >> '"';

    const auto setPath = [](auto& ctx)
    {  
        if (CodeGenConfig::templatePathName == _attr(ctx).first)
        {
            _val(ctx).templatePath = _attr(ctx).second;
        }
        else
        if (CodeGenConfig::destinationPathName == _attr(ctx).first)
        {
            _val(ctx).destinationPath = _attr(ctx).second;
        }
    };

    const auto kara_config_def =
    '{'
    >> record[setPath] >> ','
    >> record[setPath]
    >> '}';

    BOOST_SPIRIT_DEFINE(fspath, record, kara_config);

    template <class ForwardIterator, class Container>
    void config_parse(ForwardIterator begin, ForwardIterator end, Container& container)
    {
        while (begin != end)
        {
            x3::phrase_parse(begin, end, *kara_config, x3::ascii::space, container);
            
            if (begin != end)
            {
                ++begin;
            }
        }
    }

} /* namespace parser */
} /* namespace kara */