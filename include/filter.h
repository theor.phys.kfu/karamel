#pragma once

#include "typedef.h"
#include <string>
#include <vector>

namespace kara
{

class Filter
{
public:
    using FilterValues = std::vector<VarT>;

    Filter() = default;

    std::string filterId;
    FilterValues filterValues;
};

using Filters = std::vector<Filter>;

} /* namespace kara */