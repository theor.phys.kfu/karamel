#pragma once

#include <boost/spirit/home/x3.hpp>
#include <boost/algorithm/string/trim.hpp>

#include "typedef.h"

namespace kara
{
namespace parser
{
    namespace x3 = boost::spirit::x3;
    using x3::char_;
    using x3::int_;
    using x3::double_;
    using x3::alnum;
    using x3::lexeme;
    using x3::lit;
   
    const x3::rule<class key_word, std::string> key_word = "key_word";
    const x3::rule<class quoted_text, std::string> quoted_text = "quoted_text";
    const x3::rule<class vecf, std::vector<double>> vecf = "vecf";
    const x3::rule<class veci, std::vector<int>> veci = "veci";
    const x3::rule<class vecs, std::vector<std::string>> vecs = "vecs";
    const x3::rule<class var, VarT> var = "var";

    const auto key_word_def =  lexeme[ +(alnum | char_('_')) ];

    const auto trim = [](auto&& ctx) { _val(ctx) = boost::trim_copy(_attr(ctx)); };

    const auto quoted_text_def = lexeme['"' >> *(char_ - '"') >> '"'][trim];

    const auto vecf_def = '{' >> double_ % ',' >> '}';

    const auto veci_def = '{' >> int_ % ',' >> '}';

    const auto vecs_def = '{' >> quoted_text % ',' >> '}';

    const auto var_def =  lexeme[double_ >> 'f'] | int_ | quoted_text | veci | vecf | vecs;

    BOOST_SPIRIT_DEFINE(key_word, quoted_text, vecf, veci, vecs, var);

} /* namespace parser */    
} /* namespace kara */