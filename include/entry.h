#pragma once 

#include <iostream>
#include <unordered_map>
#include <list>
#include <string>

#include "typedef.h"

namespace kara
{
    
using Entry = std::unordered_map<std::string, VarT>;

using Entries = std::list<Entry>;

std::ostream& operator<<(std::ostream& out, const Entry& entry);

std::ostream& operator<<(std::ostream& out, const Entries& entries);

} /* namespace kara */
