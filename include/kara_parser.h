#pragma once

#include "common_parsers.h"

#include "entry.h"

namespace kara
{
namespace parser
{    
    const x3::rule<class field, std::pair<std::string, VarT>> field = "field";
    const x3::rule<class entry, Entry> entry = "entry";

    const auto field_def =  key_word >> '=' >> var;

    const auto entry_def = '{' >> (field % ',') >> *lit(',') >> '}';

    BOOST_SPIRIT_DEFINE(field, entry);

    template <class ForwardIterator, class Container>
    void kara_parse(ForwardIterator begin, ForwardIterator end, Container& container)
    {
        while (begin != end)
        {
            x3::phrase_parse(begin, end, *entry, x3::ascii::space, container);
            
            if (begin != end)
            {
                ++begin;
            }
        }
    }
} /* namespace parser */    
} /* namespace kara */