#pragma once

#include <string>
#include <string_view>
#include <variant>

#include "typedef.h"
#include "filter.h"
#include "format.h"

namespace kara
{

enum class MacroType
{
    K_EXPAND_FOR,
    K_COUNT_OF
};

template <MacroType>
struct MacroName;

template <>
struct MacroName<MacroType::K_EXPAND_FOR>
{
    static constexpr std::string_view name = "K_EXPAND_FOR";
};

template <>
struct MacroName<MacroType::K_COUNT_OF>
{
    static constexpr std::string_view name = "K_COUNT_OF";
};

class Position
{
public:
    Position() = default;

    Position(constIterator _begin, constIterator _end)
        : begin{_begin}, end{_end}
    {}

    constIterator begin;
    constIterator end;
};

class MacroBase
{
public:
    MacroBase() = default;

    Position position;
    Filters filters;
};

class MacroExpandFor : public MacroBase
{
public:
    MacroExpandFor() = default;

    Position templatePos;
    Formats formats;
    std::string delimiter;
};

class MacroCountOf : public MacroBase
{
public:
    MacroCountOf() = default;
};

class ExpandedMacro
{
public:
    ExpandedMacro() = default;

    ExpandedMacro(const Position& _position, const std::string& _body)
        : position{_position}, body{_body}
    {}

    Position position;
    std::string body;
};

using Macro = std::variant<MacroCountOf, MacroExpandFor>;

} /* namespace kara */