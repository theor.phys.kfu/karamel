#pragma once

#include <list>

#include "common_parsers.h"
#include "format_parser.h"
#include "filter_parser.h"
#include "macro.h"

namespace kara
{
namespace parser
{
    using x3::eps;

    const x3::rule<class text, std::string> templ = "templ";
    const x3::rule<class macro_expand_for, MacroExpandFor> macro_expand_for = "macro_expand_for";
    const x3::rule<class macro_countof, MacroCountOf> macro_countof = "macro_countof";

    const auto templ_def = +~char_("()") | (char_('(') >> *templ >> char_(')'));

    template <MacroType type>
    const auto setMacroBegin = [](auto& ctx)
    {
        _val(ctx).position.begin = _where(ctx).begin();
    };

    const auto setMacroEnd = [](auto& ctx) { _val(ctx).position.end = _where(ctx).begin(); };

    const auto setTemplateBegin = [](auto& ctx) { _val(ctx).templatePos.begin = _where(ctx).begin(); };

    const auto setTemplateEnd = [](auto& ctx) { _val(ctx).templatePos.end = _where(ctx).begin(); };

    const auto setFormats = [](auto& ctx) { _val(ctx).formats = _attr(ctx); };

    const auto setFilters = [](auto& ctx) { _val(ctx).filters = _attr(ctx); };

    const auto setDelimiter = [](auto& ctx) { _val(ctx).delimiter = _attr(ctx); };

    const auto macro_expand_for_def =
    eps[setMacroBegin<MacroType::K_EXPAND_FOR>]
    >> lit(MacroName<MacroType::K_EXPAND_FOR>::name.data())
    >> '(' >> (lit("All") | filters[setFilters]) >> ')'
    >> lexeme['(' >> eps[setTemplateBegin] >> *templ >> eps[setTemplateEnd] >> ')']
    >> -('[' >> quoted_text[setDelimiter] >> ']')
    >> lit("ALIGNMENT")
    >> ('(' >> -(lit("None") | formats[setFormats]) >> ')')[setMacroEnd];

    const auto macro_countof_def =
    eps[setMacroBegin<MacroType::K_COUNT_OF>]
    >> lit(MacroName<MacroType::K_COUNT_OF>::name.data())
    >> ('(' >> (lit("All") | filters[setFilters]) >> ')')[setMacroEnd];

    BOOST_SPIRIT_DEFINE(templ, macro_expand_for, macro_countof);

    inline std::list<Macro> getMacros(constIterator begin, constIterator end)
    {
        std::list<Macro> macros;

        while (begin != end)
        {
            x3::phrase_parse(begin, end, *(macro_expand_for | macro_countof), x3::ascii::space, macros);
            
            if (begin != end)
            {
                ++begin;
            }
        }

        return macros;
    }

} /* namespace parser */
} /* namespace kara */
