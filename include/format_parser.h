#pragma once 

#include "common_parsers.h"
#include "format.h"

namespace kara
{
namespace parser
{
    using x3::ulong_;

    const x3::rule<class format, Format> format = "format";
    const x3::rule<class formats, Formats> formats = "formats";

    const auto setWidth = [](auto& ctx)
    {
        _val(ctx).isAuto = false;
        _val(ctx).width = _attr(ctx);
    };

    const auto setAlign = [](auto& ctx)
    {
        if ('R' == _attr(ctx))
        {
            _val(ctx).align = Align::right;
        }
        else if ('L' == _attr(ctx))
        {
            _val(ctx).align = Align::left;
        }
    };

    const auto format_def = -ulong_[setWidth] >> char_("RL")[setAlign];

    const auto formats_def = format % ',' >> -lit(',');

    BOOST_SPIRIT_DEFINE(format, formats);

} /* namespace parser */
} /* namespace kara */